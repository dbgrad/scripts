echo off
echo Starting tomcat container, can take approximately 1 minute to complete, please wait...
docker run -p 8090:8080 -dit --name t9s-dbda -v C:\Users\Graduate\vm_share\tomcat-webapps\dbanalyzer.war:/usr/local/tomcat/webapps/dbanalyzer.war tomcat9-server
timeout 30 /NOBREAK

rem docker cp dbanalyzer.war t9s_dsb:/opt/tomcat/webapps

echo ..
echo ..
echo tomcat should now be available
echo
